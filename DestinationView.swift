//
//  DestinationView.swift
//  Dragging
//
//  Created by Masatoshi Nishikata on 16/02/17.
//  Copyright © 2017 Catalystwo New Zealand. All rights reserved.
//

import Foundation
import UIKit

class DestinationView: UIView, DraggingDestination {

  deinit {
    print("* deinit DestinationView *")
  }
  
  func draggingObjectEntered(_ obj: DraggingObject) {
    if let tooltip = obj.userInfo["tooltip"] as? DraggingTooltipView {
      tooltip.show(at: obj.location, in: self.superview!, message: "Dropping", type: .command)
    }
    
    obj.setTemporaryDraggingImage(UIImage(named:"hint2")!, offsetFromLocation: obj.imageOffset)
  }
  
  func draggingDestinationUpdated(_ obj: DraggingObject) {
    if let tooltip = obj.userInfo["tooltip"] as? DraggingTooltipView {
      tooltip.show(at: obj.location, in: self.superview!, message: "Dropping", type: .command)
    }
  }
  
  func draggingObjectExited(_ obj: DraggingObject) {
  obj.restoreDraggingImage()
  }
  
  func canAccept(_ draggingObject: DraggingObject) -> Bool {
    return true
  }
  
  func draggingObject(_ draggingObject: DraggingObject, locationInView point: CGPoint, imageOffsetFromLocation offset: CGPoint) -> CGPoint {
    var point = point
    point.x = round(point.x / 30) * 30
    point.y = round(point.y / 30) * 30

    return point
  }

  func draggingObjectDidDrop(_ draggingObject: DraggingObject, onCompletion handler: ((Bool) -> Void)) {
    
    handler(true)
  }
}

