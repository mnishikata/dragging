//
//  DragGestureRecognizer.swift
//  Dragging
//
//  Created by Masatoshi Nishikata on 16/02/17.
//  Copyright © 2012-2017 Catalystwo New Zealand. All rights reserved.
//

import Foundation
import UIKit

class DragGestureRecognizer: UIGestureRecognizer {
  
  var allowableMovement: CGFloat = 20
  
  var minimumPressDuration_: TimeInterval = 0.5
  var minimumHintDuration_: TimeInterval = 0.25
  
  var draggingObject: DraggingObject?
  
  var longpressTimer: Timer? = nil
  var hintTimer: Timer? = nil
  
  var initialLocationInView: CGPoint? = nil
  
  //	NSUInteger touchHash;
  var lastLocationInView: CGPoint? = nil
  var dragBegan = false
  
  
  convenience init() {
    self.init(target: nil, action: nil)
  }
  
  func update() {
    if self.state == .changed { self.state = .changed }
  }
  
  func cancel() {
    self.state = .cancelled
  }
  
  func resetTimer() {
    hintTimer?.invalidate()
    hintTimer = nil
    
    longpressTimer?.invalidate()
    longpressTimer = nil
  }
  
  func longpress(timer: Timer?) {
    self.state = .began
    longpressTimer?.invalidate()
    longpressTimer = nil
  }
  
  func hint(timer: Timer) {
    self.state = .possible
    hintTimer?.invalidate()
    hintTimer = nil
  }
  
  override func location(in view: UIView?) -> CGPoint {
    guard let lastLocationInView = lastLocationInView else { return .zero } // UNKNOWN ERR
    guard view != nil else { return .zero } // UNKNOWN ERR
    
    if view == self.view { return lastLocationInView }
    return self.view!.convert(lastLocationInView, to: view)
  }
  
  var location: CGPoint {
    guard let lastLocationInView = lastLocationInView else { return .zero } // UNKNOWN ERR
    guard let draggingObject = draggingObject else { return .zero }
    if self.view is UIWindow { return lastLocationInView }
    return self.view!.convert(lastLocationInView, to: draggingObject.parentView)
  }
  
  func lookForDestination(at fingerLocation: CGPoint) -> Bool {
    guard let draggingObject = draggingObject else { return false }
    if draggingObject.limitedInSource {
      draggingObject.destination = nil;
      return true
    }
    
    var destination: UIView? = nil
    destination = draggingObject.parentView.hitTest( fingerLocation, with:nil)
    
    while (destination != nil) && (destination is DraggingDestination) != true {
      destination = destination!.superview
    }

    var onSource = false
    var canAccept = false
    if let destination = destination as? DraggingDestination {
      canAccept = destination.canAccept(draggingObject)
    }
    
    if canAccept  {
      draggingObject.destination = destination as? DraggingDestination
    }
    else {
      if destination === draggingObject.source { onSource = true }
      draggingObject.destination = nil
    }
    
    return onSource
  }
  
  override var state: UIGestureRecognizerState {
    set {
      func finaliseDragging(success: Bool) {
        
        if( success ) { super.state = .ended }
        else { super.state = .cancelled }
        
        if( dragBegan ) {
          NotificationCenter.default.post(name: DraggingEndedNotificationName, object: nil)
        }
        draggingObject?.concludeDragging(success: success)
        draggingObject = nil
        dragBegan = false
        self.reset()
      }
      
      let location = self.location
      draggingObject?.location = location
      
      if let destination = draggingObject?.destination as? UIView {
        
        let locationInView = destination.convert(location, from: draggingObject!.parentView)
        if let newLocationInView = draggingObject!.destination?.draggingObject(draggingObject!,
                                                                               locationInView: locationInView,
                                                                               imageOffsetFromLocation: draggingObject!.imageOffset) {
          
          draggingObject!.location = destination.convert(newLocationInView, to: draggingObject!.parentView)
        }
      }
      
      switch newValue {
      case .possible:
        super.state = newValue
        draggingObject?.source?.willBeginDraggingSoon(draggingObject!)
      case .began:
        super.state = newValue
        
        dragBegan = true
        
        draggingObject?.source?.beganDragging(draggingObject!)
        draggingObject?.appearAnimation()
        
        NotificationCenter.default.post(name: DraggingStartedNotificationName, object: nil)
        
      case .changed:
        super.state = newValue
        
        let destBuffer = draggingObject?.destination
        let loc = self.location(in: draggingObject!.parentView)
        let _ =	self.lookForDestination(at: loc);
        
        // Send exit , enter  or update message
        if( draggingObject?.destination !== destBuffer ) {
          destBuffer?.draggingObjectExited(draggingObject!)
          draggingObject?.destination?.draggingObjectEntered(draggingObject!)
          
        }else {
          draggingObject?.destination?.draggingDestinationUpdated(draggingObject!)
        }
        
        draggingObject?.source.draggingSourceUpdated(draggingObject!)

      case .ended:
        guard let draggingObject = draggingObject else { super.state = .ended; return }
        // Ask destination and decide end or cancelled
        
        // Customize snapback
        // 1. Current latest point
        draggingObject.adjustSlidebackLocation()
        
        // 2.
        if let pointInView = draggingObject.source.draggingObjectNeedsCustomSlideBackPointInView(draggingObject) {
          
          let imageOffset = draggingObject.imageOffset
          let imageOrigin = CGPoint(x: pointInView.x + imageOffset.x, y: pointInView.y + imageOffset.y)
          if let source = draggingObject.source as? UIView {
            draggingObject.slidebackOrigin = source.convert(imageOrigin, to: draggingObject.parentView)
          }
        }
        
        super.state = .changed // Pending
        
        if let destination = draggingObject.destination {
          destination.draggingObjectDidDrop(draggingObject, onCompletion: finaliseDragging)
        }else {
          
          super.state = .cancelled
          finaliseDragging(success: false)
          dragBegan = false
        }

        //Finishing

      case .cancelled: fallthrough
      case .failed:
        draggingObject?.restoreDraggingImage()
        finaliseDragging(success: false)
        dragBegan = false
        super.state = newValue
      }
    }
    get {
      return super.state
    }
  }
  
  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent) {
    guard let sourceView = self.view as? DraggingSource else { return }
    if touches.count > 1 || self.state.rawValue >= UIGestureRecognizerState.began.rawValue {
      touches.forEach { self.ignore($0, for: event) }
    }
    
    if let point = touches.first?.location(in: self.view) {
      initialLocationInView = point
    }
    lastLocationInView = initialLocationInView
    
    self.state = .possible
    
    resetTimer()
    
    let result = sourceView.canBeginDragging(from: initialLocationInView!)
    
    if result == true {
      draggingObject = DraggingObject()
      
      draggingObject!.parentView = self.view!.window
      draggingObject!.dragGesture = self
      draggingObject!.initialDraggingLocation = self.location
      draggingObject!.source = sourceView
    } else {
      
      touches.forEach { self.ignore($0, for: event) }
      return
    }
    
    if sourceView.canBeginDraggingImmediately(from: initialLocationInView!) {
      super.touchesBegan(touches, with: event)
      self.state = .possible
      longpress(timer: nil)
    }else {
      
      hintTimer = Timer.scheduledTimer(timeInterval: minimumHintDuration_, target: self, selector: #selector(DragGestureRecognizer.hint(timer:)), userInfo: nil, repeats: false)
      
      longpressTimer = Timer.scheduledTimer(timeInterval: minimumPressDuration_, target: self, selector: #selector(DragGestureRecognizer.longpress(timer:)), userInfo: nil, repeats: false)
      
      super.touchesBegan(touches, with: event)
    }
  }
  
  func DISTANCE(_ n1: CGPoint, _ n2: CGPoint) -> CGFloat {
    return sqrt((n2.x - n1.x)*(n2.x - n1.x) + (n2.y - n1.y)*(n2.y - n1.y))
  }
  
  var overAllowableMovement: Bool {
    let location = self.location(in: self.view)
    
    let distance = DISTANCE( initialLocationInView!, location)
    if( distance > allowableMovement ) { return true }
    
    return false
  }
  
  override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent) {
    if longpressTimer != nil {
      
      if( overAllowableMovement && self.state != .cancelled ) {
        resetTimer()
        self.state = .cancelled
      }
      
    }else {
      self.state = .changed
    }
    
    if( self.state.rawValue <= UIGestureRecognizerState.changed.rawValue )  {
      for aTouch in touches  {
        if aTouch.gestureRecognizers?.contains(self) == true {
          lastLocationInView = aTouch.location(in: self.view)
          break
        }
      }
    }
    
    super.touchesMoved(touches, with: event)
  }
  
  override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent) {
    if longpressTimer != nil {
      resetTimer()
      self.state = .failed
    }
    else if self.state !=  .cancelled && self.state != .failed && self.state != .ended {
      self.state = .ended
    }
    
    super.touchesEnded(touches, with: event)
  }
  
  override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent) {
    resetTimer()
    if self.state != .failed && self.state != .ended {
      self.state = .cancelled
    }
    
    super.touchesCancelled(touches, with: event)
  }
  
  override func reset() {
    super.reset()
    resetTimer()
    draggingObject = nil
    dragBegan = false
  }
  
  override func canBePrevented(by preventingGestureRecognizer: UIGestureRecognizer) -> Bool {
    if self.state.rawValue >=  UIGestureRecognizerState.changed.rawValue { return false }
    return true
  }
  
  override func canPrevent(_ preventedGestureRecognizer: UIGestureRecognizer) -> Bool {
    if preventedGestureRecognizer.isMember(of: UILongPressGestureRecognizer.self) {
      return true
    }
    
    if preventedGestureRecognizer.isKind(of: UISwipeGestureRecognizer.self) {
      let theirLocation = preventedGestureRecognizer.location(in: self.view)
      let myLocation = self.location(in: self.view)
      if( DISTANCE(myLocation, theirLocation) < 10 ) { return true }
    }
    
    if self.state.rawValue >= UIGestureRecognizerState.changed.rawValue {
      //相手が２本指以上のタッチは阻止
      if preventedGestureRecognizer.numberOfTouches > 1 { return true }
      
      let theirLocation = preventedGestureRecognizer.location(in: self.view)
      if theirLocation.equalTo(self.location(in:self.view ) )  { return true }
      return false
    }
    
    return false
  }
}
