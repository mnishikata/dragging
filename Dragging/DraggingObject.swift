//
//  DraggingObject.swift
//  Dragging
//
//  Created by Masatoshi Nishikata on 16/02/17.
//  Copyright © 2017 Catalystwo New Zealand. All rights reserved.
//

import Foundation
import UIKit

class DraggingLayer: CALayer {
}

public class DraggingObject: NSObject, CAAnimationDelegate {
  var userInfo: [String: Any] = [:]
  var source: DraggingSource! {
    didSet {
      if let view = source as? UIView, let origin = view.superview?.convert(view.frame.origin, to: self.parentView) {
        initialSourceOrigin = origin
      }
    }
  }
  var destination: DraggingDestination?
  
  var parentView: UIView!
  weak var dragGesture: DragGestureRecognizer?
  
  var limitedInSource = false
  
  var	initialDraggingLocation: CGPoint = .zero {
    didSet {
      location = initialDraggingLocation
      slidebackOrigin = initialDraggingLocation
    }
  }
  var initialSourceOrigin: CGPoint = .zero
  var	location: CGPoint = .zero {
    didSet {
      if draggingLayer.superclass != nil && isEnding == false {
        draggingLayer.position = location
      }
    }
  }
  var animateDropping = true
  var success = false
  
  
  private lazy var draggingLayer: DraggingLayer = {
    let layer = DraggingLayer()
    layer.actions = ["onOrderIn": NSNull(), "onOrderOut": NSNull(), "sublayers": NSNull(), "position": NSNull() ]
    return layer
  }()
  
  var imageOffset: CGPoint = .zero
  var slidebackOrigin: CGPoint = .zero
  
  private var isEnding = false

  private var originalImage: UIImage!
  private var originalImageLocation: CGPoint = .zero
  
  private var isTemporaryImage_ = false
  
  
  class func clearDragging(in view: UIView) {
    var draggingLayers: [CALayer] = []
    view.layer.sublayers?.forEach { layer in
      
      if layer is DraggingLayer {
        draggingLayers.append(layer)
      }
    }
    draggingLayers.forEach { $0.removeFromSuperlayer() }
  }
  
  deinit {
    draggingLayer.removeFromSuperlayer()
    print("* deinit DraggingObject *")

  }
  
  func setDraggingImage(_ image: UIImage, at origin: CGPoint, inView view: UIView? = nil) {
    var origin = origin
    if image != originalImage {
      originalImage = image
    }
    
    if view != nil {
      origin = view!.convert(origin, to:self.parentView)
    }
    
    originalImageLocation = origin
    
    if isEnding { return }
    
    draggingLayer.contents = originalImage.cgImage
    
    imageOffset = CGPoint(x: location.x - origin.x, y: location.y - origin.y)
    draggingLayer.frame = CGRect(origin: origin, size: image.size)
    draggingLayer.anchorPoint = CGPoint(x: imageOffset.x / image.size.width, y: imageOffset.y / image.size.height)
    
    draggingLayer.position = location
    
    parentView.layer.addSublayer(draggingLayer)
  }
  
  func setTemporaryDraggingImage(_ image: UIImage, offsetFromLocation: CGPoint) {
    if isEnding { return }
    
    draggingLayer.contents = image.cgImage
    
    draggingLayer.frame = CGRect(x: location.x - offsetFromLocation.x, y: location.y - offsetFromLocation.y, width: image.size.width, height: image.size.height)
    draggingLayer.anchorPoint = CGPoint(x: offsetFromLocation.x / image.size.width, y: offsetFromLocation.y / image.size.height)
    
    draggingLayer.position = location
    
    isTemporaryImage_ = true
  }
  
  func restoreDraggingImage() {
    if isTemporaryImage_ {
      setDraggingImage(originalImage, at: CGPoint(x: location.x - imageOffset.x, y: location.y - imageOffset.y ))
    }
  }
  
  func location(in view: UIView) -> CGPoint {
    return parentView.convert(location, to: view)
  }
  
  func appearAnimation() {
    let t0 = CATransform3DMakeScale(1, 1, 1)
    let t1 = CATransform3DMakeScale(1.1, 1.1, 1)
    
    let animation = CABasicAnimation()
    animation.keyPath = "transform"
    animation.fromValue = NSValue(caTransform3D: t0)
    animation.toValue = NSValue(caTransform3D: t1)
    animation.duration = 0.2
    animation.isRemovedOnCompletion = false
    // leaves presentation layer in final state; preventing snap-back to original state
    animation.fillMode = kCAFillModeBoth
    animation.repeatCount = 0
    animation.autoreverses = true
    animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseIn)
    //animation.delegate = self
    
    draggingLayer.add(animation, forKey: "appear")
  }
  
  func dropAnimation() {
    isEnding = true
    
    let t0 = CATransform3DMakeScale(1, 1, 1)
    let t1 = CATransform3DMakeScale(0.01, 0.01, 0.01)
    
    draggingLayer.transform = t1
    
    let animation = CABasicAnimation()
    animation.keyPath = "transform"
    animation.fromValue = NSValue(caTransform3D: t0)
    animation.toValue = NSValue(caTransform3D: t1)
    animation.duration = 0.35
    animation.isRemovedOnCompletion = false
    // leaves presentation layer in final state; preventing snap-back to original state
    animation.fillMode = kCAFillModeBoth
    animation.repeatCount = 0
    animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseIn)
    animation.delegate = self
    
    draggingLayer.add(animation, forKey: "drop")
  }
  
  func adjustSlidebackLocation() {
    guard let source = source as? UIView else { return }
    if( source.superview == nil || source.isHidden )
    {
      slidebackOrigin.x = location.x
      slidebackOrigin.y = -10 - originalImage.size.height
      return
    }
    
    if let sourceOrigin = source.superview?.convert( source.frame.origin , to: parentView) {
      slidebackOrigin = sourceOrigin
    }
    
    slidebackOrigin.x -= (initialSourceOrigin.x - initialDraggingLocation.x)
    slidebackOrigin.y -= (initialSourceOrigin.y - initialDraggingLocation.y)
  }
  
  func slideback() {
    isEnding = true
    draggingLayer.position = slidebackOrigin
    
    let animation = CABasicAnimation()
    animation.keyPath = "position"
    animation.fromValue = NSValue(cgPoint: location)
    animation.toValue = NSValue(cgPoint: slidebackOrigin)
    animation.duration = 0.35
    animation.isRemovedOnCompletion = false
    // leaves presentation layer in final state; preventing snap-back to original state
    animation.fillMode = kCAFillModeBoth
    animation.repeatCount = 0
    animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
    animation.delegate = self
    
    draggingLayer.add(animation, forKey: "slideback")
  }
  
  func concludeDragging(success: Bool) {
    self.success = success
    self.source.concludeDragging(self, success: success)
    
    pendingDraggingObject = self // (*)
    if success {
      if animateDropping { dropAnimation() }
      else { finaliseAnimation() }
    }else {
      slideback()
    }
    
    destination = nil
  }
  
  public func animationDidStart(_ anim: CAAnimation) {
    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5)  {
      self.callDidStartDelegateMethod()
    }
  }
  
  func callDidStartDelegateMethod() {
    self.source.concludeDraggingAnimationDidStart(self)
  }
  
  public func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
    finaliseAnimation()
  }
  
  func finaliseAnimation() {
    draggingLayer.removeAllAnimations()
    draggingLayer.removeFromSuperlayer()
    self.source.concludeDraggingAnimationDidStart(self)
    
    pendingDraggingObject = nil // (*)
  }
}

var pendingDraggingObject: DraggingObject? = nil
