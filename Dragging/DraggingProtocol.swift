//
//  DraggingProtocol.swift
//  Dragging
//
//  Created by Masatoshi Nishikata on 16/02/17.
//  Copyright © 2012-2017 Catalystwo New Zealand. All rights reserved.
//

import Foundation
import UIKit

var DraggingStartedNotificationName = NSNotification.Name(rawValue: "MNDraggingStartedNotificationName")
var DraggingEndedNotificationName = NSNotification.Name(rawValue: "MNDraggingEndedNotificationName")


protocol DraggingSource: NSObjectProtocol {
  /*
   var superview: UIView? { get }
   var frame: CGRect { get }
   var isHidden: Bool { get }
   */
  // Source returns slideback point in window
  
  func canBeginDraggingImmediately(from locationInView: CGPoint) -> Bool
  func canBeginDragging(from locationInView: CGPoint) -> Bool
  func willBeginDraggingSoon(_ draggingObject: DraggingObject)
  // Tell source to begin.  Souce can change image
  func beganDragging(_ draggingObject: DraggingObject)
  
  func draggingObjectNeedsCustomSlideBackPointInView(_ draggingObject: DraggingObject) -> CGPoint?
  // This method is called periodically
  func draggingSourceUpdated(_ draggingObject: DraggingObject)
  // Tell source the results
  func concludeDragging(_ draggingObject: DraggingObject, success: Bool)
  func concludeDraggingAnimationDidStart(_ draggingObject: DraggingObject)
  func concludeDraggingAnimationDidFinish(_ draggingObject: DraggingObject)
  
}

protocol DraggingDestination: NSObjectProtocol {
  
  func canAccept(_ draggingObject: DraggingObject) -> Bool
  func draggingObjectEntered(_ draggingObject: DraggingObject)
  func draggingDestinationUpdated(_ draggingObject: DraggingObject)
  func draggingObjectExited(_ draggingObject: DraggingObject)
  func draggingObjectDidDrop(_ draggingObject: DraggingObject, onCompletion: ((Bool) -> Swift.Void))
  
  func draggingObject(_ draggingObject: DraggingObject, locationInView: CGPoint, imageOffsetFromLocation: CGPoint) -> CGPoint
}
