//
//  DraggingTooltipView.swift
//  Dragging
//
//  Created by Masatoshi Nishikata on 16/02/17.
//  Copyright © 2012-2017 Catalystwo New Zealand. All rights reserved.
//

import Foundation
import UIKit

enum TooltipViewType {
  case information, command, destructive
}


class DraggingTooltipView: UIView {
  var tooltipLayer_: CALayer!
  
  var text: String = ""
  var textSize: CGSize!
  
  var tooltipType: TooltipViewType!
  var tooltipLocation: CGPoint!
  var size: CGSize!
  
  
  // CONSTANTS
  
  let MAX_WIDTH: CGFloat = 250
  let ALPHA: CGFloat = 0.9
  let FONT: UIFont = UIFont.systemFont(ofSize:16)
  let ABOVE: CGFloat = 35
  
  let SHOW_ANIMATION: TimeInterval = 0.2
  
  let INDICATOR_HEIGHT: CGFloat = 15
  let INDICATOR_WIDTH_2: CGFloat = 10
  let INDICATOR_SIDE_MARGIN: CGFloat = 25
  let TOOLTIP_EDGE_MARGIN: CGFloat = 10
  let STATUS_BAR_HEIGHT: CGFloat = 20
  
  //
  
  convenience init() {
    let frame = CGRect(x: 0, y: 0, width: 200, height: 50)
    self.init(frame: frame)
    
    tooltipLayer_ = CALayer()
    tooltipLayer_.frame = frame
    tooltipLayer_.delegate = self
    tooltipLayer_.backgroundColor = UIColor.clear.cgColor
    tooltipLayer_.isOpaque = false
    tooltipLayer_.opacity = 0
    tooltipLayer_.zPosition = 22
    tooltipLayer_.contentsScale = UIScreen.main.scale
  }
  
  deinit {
    tooltipLayer_.removeFromSuperlayer()
    print("* deinit DraggingTooltipView *")
  }
  
  func show(at location: CGPoint, in view: UIView, message: String, type: TooltipViewType, afterDelay: TimeInterval = 0) {
    
    let shouldDraw = self.text != message
    
    text = message
    tooltipType = type
    
    // Calc size
    textSize = (text as NSString).size(attributes: [NSFontAttributeName: FONT])
    
    
    var textRect = CGRect(x: 10 + TOOLTIP_EDGE_MARGIN, y: 2+TOOLTIP_EDGE_MARGIN, width: textSize.width, height: textSize.height)
    
    
    textRect = textRect.integral
    
    var viewRect = CGRect(x: 0, y: 0, width: textRect.size.width + 20, height: textRect.size.height + 4)
    
    // Add margin for shadow+ fukidashi
    viewRect.origin.x = viewRect.origin.x - TOOLTIP_EDGE_MARGIN
    viewRect.origin.y = viewRect.origin.y - TOOLTIP_EDGE_MARGIN
    viewRect.size.width += TOOLTIP_EDGE_MARGIN*2
    viewRect.size.height += TOOLTIP_EDGE_MARGIN*2 + INDICATOR_HEIGHT
    
    if location.equalTo(.zero) {
      viewRect.origin.y = tooltipLayer_.frame.origin.y
      viewRect.origin.x = tooltipLayer_.frame.origin.x + viewRect.size.width/2
    }else {
      viewRect.origin.y = location.y - viewRect.size.height - ABOVE
      viewRect.origin.x = location.x - viewRect.size.width/2
    }
    
    // Fit in Window
    
    
    let containRect = view.bounds
    
    if viewRect.origin.x < containRect.origin.x {
      viewRect.origin.x = containRect.origin.x
    }
    
    if viewRect.maxX > containRect.maxX {
      viewRect.origin.x = containRect.maxX - viewRect.size.width
    }
    
    if viewRect.origin.y < containRect.origin.y  {
      viewRect.origin.y = containRect.origin.y
    }
    
    if viewRect.maxY > containRect.maxY  {
      viewRect.origin.y = containRect.maxY - viewRect.size.height
    }
    
    viewRect = viewRect.integral
    tooltipLayer_.opacity = 1.0
    tooltipLayer_.transform = CATransform3DIdentity
    tooltipLayer_.frame = viewRect
    self.size = viewRect.size
    
    //
    
    self.tooltipLocation = view.layer.convert(location, to:tooltipLayer_)
    if location.equalTo(.zero) {
      self.tooltipLocation = .zero
    }
    
    if shouldDraw {
      tooltipLayer_.setNeedsDisplay()
    }
    
    if tooltipLayer_.superlayer == nil {
      
      view.layer.addSublayer(tooltipLayer_)
      
      self.tooltipLocation = view.layer.convert(location, to:tooltipLayer_)
      if location.equalTo(.zero) {
        self.tooltipLocation  = .zero
      }
      
      let centerToLocation = CGPoint(x: self.tooltipLocation.x - viewRect.size.width/2,
                                     y: self.tooltipLocation.y - ABOVE-TOOLTIP_EDGE_MARGIN - viewRect.size.height/2)
      
      let firstDuration: CFTimeInterval = 0.3
      
      let animation = CABasicAnimation()
      animation.keyPath = "transform"
      var t0 = CATransform3DIdentity
      
      t0 = CATransform3DTranslate(t0, centerToLocation.x-centerToLocation.x * 0.1, centerToLocation.y-centerToLocation.y * 0.1 , 0)
      t0 = CATransform3DScale(t0, 0.1, 0.1, 1)
      
      animation.fromValue = NSValue(caTransform3D: t0)
      animation.toValue = NSValue(caTransform3D: CATransform3DIdentity)
      animation.duration = firstDuration
      animation.isRemovedOnCompletion = true
      animation.fillMode = kCAFillModeRemoved
      animation.repeatCount = 1
      
      let function = CAMediaTimingFunction(controlPoints: 0.8, 2.3, 0.6, 0.5)
      
      animation.timingFunction = function
      tooltipLayer_.add(animation, forKey: "appear1")
      
    }
  }
  
  func closeTooltip() {
    UIView.animate(withDuration: SHOW_ANIMATION, delay: 0, options: [.curveEaseInOut], animations: {
      self.tooltipLayer_.opacity = 0
      
    }, completion: { success in self.tooltipLayer_.removeFromSuperlayer()  })
    
  }
  
  func RGBA(_ r: CGFloat,_ g: CGFloat,_ b: CGFloat,_ a: CGFloat) -> UIColor  {
    return UIColor(red: r/255.0, green: g/255.0, blue: b/255.0, alpha: a)
  }
  
  override func draw(_ layer: CALayer, in ctx: CGContext) {
    let rect = layer.bounds
    ctx.saveGState()
    
    if( self.tooltipType == .command )
    {
      ctx.setFillColor(RGBA(0,0,0,1).cgColor)
      
    }
    else if( self.tooltipType == .destructive )
    {
      ctx.setFillColor(RGBA(178,0,0,1).cgColor)
      
    }
    else//if( self.tooltipType == TooltipViewInformation )
    {
      ctx.setFillColor(RGBA(0,0,0,0.4).cgColor)
    }
    
    ctx.setStrokeColor(RGBA(178,178,178,1).cgColor)
    ctx.setLineWidth(1.5)
    
    // tooltipLocation
    
    var location = self.tooltipLocation!
    var bodyRect = rect
    
    if location.equalTo(CGPoint.zero) == false {
      
      if( location.x < rect.origin.x + INDICATOR_SIDE_MARGIN + TOOLTIP_EDGE_MARGIN ) {
        location.x = rect.origin.x + INDICATOR_SIDE_MARGIN + TOOLTIP_EDGE_MARGIN
      }
      
      if( location.x > rect.maxX - INDICATOR_SIDE_MARGIN - TOOLTIP_EDGE_MARGIN) {
        location.x = rect.maxX - INDICATOR_SIDE_MARGIN - TOOLTIP_EDGE_MARGIN
      }
      
      location.y = rect.maxY - TOOLTIP_EDGE_MARGIN
      
      // Round
      bodyRect = CGRect(x: rect.origin.x + TOOLTIP_EDGE_MARGIN,
                        y: rect.origin.y + TOOLTIP_EDGE_MARGIN,
                        width: rect.size.width - TOOLTIP_EDGE_MARGIN*2,
                        height: rect.size.height - TOOLTIP_EDGE_MARGIN*2-INDICATOR_HEIGHT)
    }
    
    ctx.saveGState()
    var bezierPath: CGPath
    
    addFukidashiRoundedRectToPath(ctx, rect:bodyRect, ovalWidth:13, ovalHeight:13, fukidashiPoint:location ,indicatorWidth_2:10)
    
    bezierPath = ctx.path!
    ctx.restoreGState()
    
    
    ctx.addPath(bezierPath)
    ctx.drawPath(using: .fill)
    
    ctx.setShadow(offset: CGSize.zero, blur: 0, color: UIColor.clear.cgColor)
    addFukidashiRoundedRectToPath(ctx, rect:bodyRect, ovalWidth:13, ovalHeight:13, fukidashiPoint:location ,indicatorWidth_2:10)
    
    ctx.addPath(bezierPath)
    //	CGContextDrawPath(ctx, kCGPathStroke);
    
    UIGraphicsPushContext(ctx)
    UIColor.white.set()
    //	CGContextSetShadow(ctx, CGSizeMake(0, +1), 2);
    
    let drawArea = CGRect(x: bodyRect.origin.x+10, y: bodyRect.origin.y+2, width: textSize.width, height: textSize.height)
    (text as NSString).draw(in: drawArea, withAttributes: [NSFontAttributeName: FONT, NSForegroundColorAttributeName: UIColor.white])
    
    UIGraphicsPopContext()
    ctx.restoreGState()
  }
}


func addRoundedRectToPath(_ context:CGContext,  rect: CGRect, ovalWidth: CGFloat, ovalHeight: CGFloat) {
  var fw: CGFloat, fh: CGFloat
  if ovalWidth == 0 || ovalHeight == 0 {
    // 1
    context.addRect(rect)
    return
  }
  context.saveGState()
  // 2
  context.translateBy (x: rect.minX, y: rect.minY)
  // 3
  
  context.scaleBy (x: ovalWidth, y: ovalHeight)
  // 4
  fw = rect.width / ovalWidth
  // 5
  fh = rect.height / ovalHeight
  // 6
  context.move(to: CGPoint(x: fw, y: fh/2))
  // 7
  context.addArc(tangent1End: CGPoint(x: fw, y: fh), tangent2End: CGPoint(x: fw/2, y: fh), radius:1)
  // 8
  context.addArc(tangent1End: CGPoint(x: 0, y: fh), tangent2End: CGPoint(x: 0, y: fh/2), radius:1)
  // 9
  context.addArc(tangent1End: CGPoint(x: 0, y: 0), tangent2End: CGPoint(x: fw/2, y: 0), radius:1)
  // 10
  context.addArc(tangent1End: CGPoint(x: fw, y: 0), tangent2End: CGPoint(x: fw, y: fh/2), radius:1)
  // 11
  context.closePath()
  // 12
  context.restoreGState()
  // 13
}

func addFukidashiRoundedRectToPath(_ context: CGContext, rect: CGRect,  ovalWidth: CGFloat, ovalHeight: CGFloat, fukidashiPoint: CGPoint, indicatorWidth_2: CGFloat) {
  
  if( fukidashiPoint.equalTo(.zero) )
  {
    addRoundedRectToPath(context, rect:rect, ovalWidth:ovalWidth, ovalHeight:ovalHeight)
    return
  }
  
  var fw: CGFloat, fh: CGFloat
  if (ovalWidth == 0 || ovalHeight == 0) {
    // 1
    context.addRect(rect)
    return
  }
  
  // Calc p1, p2
  var p1: CGPoint, p2: CGPoint
  
  var orientation: UIDeviceOrientation = .portrait
  
  if( fukidashiPoint.y > rect.maxY ) { orientation = .portrait }
  if( fukidashiPoint.x < rect.minX ) { orientation = .landscapeLeft }
  if( fukidashiPoint.x > rect.maxX ) { orientation = .landscapeRight }
  
  switch orientation {
  case .landscapeLeft:
    p1 = CGPoint(x: rect.minX, y: fukidashiPoint.y - indicatorWidth_2 )
    p2 = CGPoint(x: rect.minX, y: fukidashiPoint.y + indicatorWidth_2 )
  case .landscapeRight:
    p1 = CGPoint(x: rect.maxX, y: fukidashiPoint.y - indicatorWidth_2 )
    p2 = CGPoint(x: rect.maxX, y: fukidashiPoint.y + indicatorWidth_2 )
  case .portrait: fallthrough
  default:
    p1 = CGPoint(x: fukidashiPoint.x - indicatorWidth_2, y: rect.maxY)
    p2 = CGPoint(x: fukidashiPoint.x + indicatorWidth_2, y: rect.maxY)
  }

  context.saveGState()
  
  // 4
  fw = rect.width
  // 5
  fh = rect.height
  // 6
  
  if( orientation == .landscapeRight ) {
    context.move(to: fukidashiPoint)
    context.addLine(to: p2)
  }
  else {
    context.move(to: CGPoint(x: fw+rect.minX, y: fh/2+rect.minY))
  }
  
  // 7
  context.addArc(tangent1End: CGPoint(x: fw+rect.minX, y: fh+rect.minY), tangent2End: CGPoint(x: fw+rect.minX-ovalWidth, y: fh+rect.minY), radius: ovalWidth)
  
  if( orientation == .portrait ) {
    context.addLine(to: p2)
    context.addLine(to: fukidashiPoint)
    context.addLine(to: p1)
  }
  
  // 8
  context.addArc(tangent1End: CGPoint(x: rect.minX, y: fh+rect.minY), tangent2End: CGPoint(x: rect.minX, y: fh/2+rect.minY), radius: ovalWidth)
  
  if( orientation == .landscapeLeft ) {
    context.addLine(to: p2)
    context.addLine(to: fukidashiPoint)
    context.addLine(to: p1)
  }
  
  // 9
  context.addArc(tangent1End: CGPoint(x: rect.minX, y: rect.minY), tangent2End: CGPoint(x: fw+rect.minX-ovalWidth, y: rect.minY), radius: ovalWidth)
  
  // 10
  
  if( orientation == .landscapeRight ) {
    context.addArc(tangent1End: CGPoint(x: fw+rect.minX, y: rect.minY), tangent2End: CGPoint(x: fw+rect.minX, y: p1.y), radius: ovalWidth)
    
    context.addLine(to: p1)
    context.addLine(to: fukidashiPoint)
    
  }else {
    context.addArc(tangent1End: CGPoint(x: fw+rect.minX, y: rect.minY), tangent2End: CGPoint(x: fw+rect.minX, y: fh/2+rect.minY), radius: ovalWidth)
  }
  
  // 11
  context.closePath()
  // 12
  context.restoreGState()
  // 13
}


