# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Dragging protocol for iPhone, similar to NSDraggingSource/NSDraggingDestination Protocols on Mac.

Originally written for CCal in ObjC in 2010.  Ported to Swift 3.

![screenshot](https://bytebucket.org/mnishikata/dragging/raw/55aca1e50c903f32b65817c0104b11120e3b3de3/Screenshot.png)