//
//  SourceView.swift
//  Dragging
//
//  Created by Masatoshi Nishikata on 16/02/17.
//  Copyright © 2012-2017 Catalystwo New Zealand. All rights reserved.
//

import Foundation
import UIKit

class SourceView: UIView, DraggingSource {
  
  var dragGesture: DragGestureRecognizer!
  var deferRemovingFromSuperview = false
  var initialPoint: CGPoint? = nil

  deinit {
    print("* deinit SourceView *")
  }
  
  func setup() {
    dragGesture = DragGestureRecognizer()
    self.addGestureRecognizer(dragGesture)
  }
  
  override func didMoveToWindow() {
    setup()
    super.didMoveToWindow()
  }
  
  override func removeFromSuperview() {
    let state = dragGesture.state
    
    if state == .began || state == .changed {
      deferRemovingFromSuperview = true
      self.isHidden = true
      dragGesture.draggingObject?.parentView.addSubview(self)
    }
    self.removeGestureRecognizer(dragGesture)
    super.removeFromSuperview()
  }
  
  func canBeginDraggingImmediately(from locationInView: CGPoint) -> Bool {
    return true
  }
  
  func canBeginDragging(from locationInView: CGPoint) -> Bool {
    initialPoint = locationInView
    return true
  }
  
  func beganDragging(_ draggingObject: DraggingObject) {
    if let p0 = draggingObject.dragGesture?.location {
      let image = UIImage(named: "hint1")!
      var point = CGPoint(x: p0.x - image.size.width / 2, y: p0.y - image.size.height / 2 )
      
      if initialPoint != nil {
        point.x -= (initialPoint!.x - self.bounds.size.width/2)
        point.y -= (initialPoint!.y - self.bounds.size.height/2)
      }
      
      draggingObject.setDraggingImage(image, at: point)
    }
    
    if let tooltip = draggingObject.userInfo["tooltip"] as? DraggingTooltipView {
      tooltip.show(at: draggingObject.location, in: draggingObject.parentView, message: "Dragging began", type: .information)
    }
  }
  
  func draggingObjectNeedsCustomSlideBackPointInView(_ draggingObject: DraggingObject) -> CGPoint? {
    return nil
  }
  
  func draggingSourceUpdated(_ draggingObject: DraggingObject) {
    if draggingObject.destination == nil {
      if let tooltip = draggingObject.userInfo["tooltip"] as? DraggingTooltipView {
        tooltip.show(at: draggingObject.location, in: draggingObject.parentView, message: "Cancel", type: .information)
      }
    }
  }
  
  func concludeDragging(_ draggingObject: DraggingObject, success: Bool) {
    if let tooltip = draggingObject.userInfo["tooltip"] as? DraggingTooltipView {
      tooltip.closeTooltip()
    }
    
    if deferRemovingFromSuperview {
      deferRemovingFromSuperview = false
      removeFromSuperview()
    }
  }
  
  func willBeginDraggingSoon(_ draggingObject: DraggingObject) {
    let tooltip = DraggingTooltipView()
    tooltip.show(at: draggingObject.location, in: draggingObject.parentView, message: "Hold to drag", type: .information)
    
    draggingObject.userInfo["tooltip"] = tooltip
  }
  
  func concludeDraggingAnimationDidStart(_ draggingObject: DraggingObject) {}
  func concludeDraggingAnimationDidFinish(_ draggingObject: DraggingObject) {}
}
